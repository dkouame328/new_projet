import React from 'react';
import { Button } from 'components/common/Button/Button';

import * as S from './pagetestons.styled';

const Testpage: React.FC = () => {
  return (
    <S.Wrapper>
      <div>
        <S.Text>Configuration page for spark</S.Text>
      </div>
      <Button buttonSize="L" buttonType="secondary">
        Valider
      </Button>
    </S.Wrapper>
  );
};

export default Testpage;
